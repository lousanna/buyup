package com.example.lousanna.buyup;

import java.util.HashMap;

/**
 * Created by emmaadams on 4/16/17.
 */

public class Item {

    public String title;
    public Integer id;
    public String price;
    public String category;
    public String condition;
    public String uid;
    public String size;
    public String color;
    public String uname;
    public String date;

    public HashMap<String, Integer> fav = new HashMap<>();
    public Long milli;

    public int fave;

    public String key;


    public Item() {
        // Empty Constructor
    }

    public Item(String title, String price, String size, String color, String category, String condition, int fave, Integer id, String uid, String uname, String date, Long milli) {
        this.title = title;
        this.price = price;
        this.category = category;
        this.condition = condition;
        this.fave = fave;
        this.uid = uid;
        this.id = id;
        this.size = size;
        this.color = color;
        this.uname = uname;
        this.date = date;
        this.milli = milli;
    }

    public Item(Item v) {
        this.title = v.title;
        this.price = v.price;
        this.category = v.category;
        this.condition = v.condition;
        this.fave = v.fave;
        this.uid = v.uid;
        this.id = v.id;
        this.size = v.size;
        this.color = v.color;
        this.uname = v.uname;
        this.date = v.date;
        this.milli = v.milli;
    }

    public void deepcopyMap(Item v) {
        this.fav.clear();
        for (String a : fav.keySet()) {
            this.fav.put(a, fav.get((a)));
        }
    }

    public Integer getUserFave(String uidIn) {

        if (fav.containsKey(uidIn)) {
            return fav.get(uidIn);
        } else {
            fav.put(uidIn, 0);
            return 0;
        }
    }

    public void toggleUserFave(String uidIn) {

        int curr = getUserFave(uidIn);
        fav.put(uidIn, Math.abs(curr - 1));
    }
}
