package com.example.lousanna.buyup;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

import static java.util.Collections.sort;

/**
 * Created by emmaadams on 4/16/17.
 */



public class MyItemsFragment extends Fragment {

    protected static ItemAdapter aa;
    private ListView logListView;
    protected static ArrayList<Item> Items;
    private Context context;
    private View rootView;

    public ArrayList<Item> log;
    public HashMap<Long, String> map = new HashMap<Long, String>();
    public Long pos;

    public FirebaseDatabase database;
    public DatabaseReference myRef;
    public FirebaseUser user;
    Integer t = 0;

    private ImageView fave;

    public MyItemsFragment() {
        // Required empty public constructor

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        context = getActivity().getApplicationContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_my_items, container, false);

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference().child("Item");

        user = FirebaseAuth.getInstance().getCurrentUser();

        logListView = (ListView) rootView.findViewById(R.id.log_list_view);

        logListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Class fclass = AddItemFragment.class;

                Bundle bundle = new Bundle();
                bundle.putInt("edit", 1);
                if (map.containsKey(id)) {
                    bundle.putString("position", map.get(id));
                }
                Fragment fragment = null;
                try {
                    fragment = (Fragment) fclass.newInstance();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                fragment.setArguments(bundle);

                getActivity().getFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, fragment)
                        .addToBackStack(null)
                        .commit();
                getActivity().setTitle("Edit Listing");
            }
        });


        pos = new Long(0);

        populateData();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        //logListView.setAdapter(null);
        populateData();

        // Populating fake data - will need to replace with getting actual data from database
        //populateData();
        getActivity().setTitle("My Items");

    }

    public void populateData() {

        if (getArguments().getInt("from") == 1) {
            logListView.setAdapter(null);
        }
        //logListView.setAdapter(null);
        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.

                log = new ArrayList<Item>();
                pos = new Long(0);


                for(DataSnapshot ds: dataSnapshot.getChildren()) {
                    Item value = ds.getValue(Item.class);

                    if (user.getEmail().equals(value.uid)) {

                        log.add(value);
                    }
                }
                sort(log, new Comparator<Item>() {
                    @Override
                    public int compare(Item o1, Item o2) {
                        return o2.milli.compareTo(o1.milli);
                    }
                });

                for (int i = 0; i < log.size(); i++) {
                    map.put((long) i, log.get(i).key);
                }

                if (logListView.getAdapter() == null) {
                    aa = new ItemAdapter(getActivity(), R.layout.my_item_list, log);
                    aa.notifyDataSetChanged();
                    logListView.setAdapter(aa);
                } else {
                    ((ItemAdapter)logListView.getAdapter()).refill(log);
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                System.out.println("Failed to read value.");
            }
        });
    }
}
