package com.example.lousanna.buyup;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

/**
 * Created by emmaadams on 4/16/17.
 */

public class ResetPassword extends AppCompatActivity {

    private View rootView;
    private ListView logListView;


    public ResetPassword() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        Button done = (Button) findViewById(R.id.done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent proceed = new Intent(ResetPassword.this, LogIn.class);
                startActivity(proceed);

            }
        });

    }
}

