package com.example.lousanna.buyup;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


public class HomeScreen extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    //NOTE:
    //This is where you can change to the login/signup fragment
    private static String currentTitle = "Home";
    private static Fragment currentFragment = new HomeScreenFragment();

    private View navHeader;
    private TextView navName;
    private TextView navDomain;
    public String name;
    public String email;
    public FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                if (slideOffset == 0) {
                    // drawer closed
                    //getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
                    invalidateOptionsMenu();
                } else if (slideOffset != 0) {

                    name = FirebaseAuth.getInstance().getCurrentUser().getDisplayName();
                    email = user.getEmail();

                    navName.setText("Welcome " + name);
                    if (!email.equals("") && email.contains(".edu")) {
                        navDomain.setText(email.substring(email.indexOf("@") + 1, email.indexOf(".edu")));
                    }
                    invalidateOptionsMenu();
                }
                super.onDrawerSlide(drawerView, slideOffset);
            }

        };
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        navHeader = navigationView.inflateHeaderView(R.layout.nav_header_home_screen);

        navName = (TextView) navHeader.findViewById(R.id.buyuptext);
        navDomain = (TextView) navHeader.findViewById(R.id.buyupemail);

        name = user.getDisplayName();
        email = user.getEmail();


        navName.setText("Welcome " + name);
        if (!email.equals("") && email.contains(".edu")) {
            navDomain.setText(email.substring(email.indexOf("@") + 1, email.indexOf(".edu")));
        }

        /*
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                Glide.get(getApplicationContext()).clearDiskCache();
            }
        });*/

        goHome();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_screen, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            currentFragment = new SettingsFragment();
            currentTitle = "Settings";
            changeFragment(currentFragment,currentTitle);
            return true;
        } else if (id == R.id.action_search) {
            Class temp = AddItemFragment.class;
            Bundle bundle = new Bundle();
            bundle.putInt("edit", 2);
            try {
                currentFragment = (Fragment) temp.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            currentFragment.setArguments(bundle);
            currentTitle = "Advanced Search";
            changeFragment(currentFragment,currentTitle);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            goHome();
        } else if (id == R.id.nav_your_items) {

            Class temp = MyItemsFragment.class;
            Bundle bundle = new Bundle();
            bundle.putInt("from", 0);
            try {
                currentFragment = (Fragment) temp.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            currentFragment.setArguments(bundle);
            currentTitle = "My Items";
        } else if (id == R.id.nav_favorites) {

            currentFragment = new FavoritesFragment();
            currentTitle = "Favorites";
        } else if (id == R.id.nav_add_item) {
            Class temp = AddItemFragment.class;
            Bundle bundle = new Bundle();
            bundle.putInt("edit", 0);
            try {
                currentFragment = (Fragment) temp.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            currentFragment.setArguments(bundle);
            currentTitle = "Add a Listing";
        } else if (id == R.id.log_out) {
            FirebaseAuth.getInstance().signOut();
            Intent intent = new Intent(this, Splash.class);
            startActivity(intent);
            return true;
        }

        changeFragment(currentFragment, currentTitle);

        return true;
    }

    public void goHome() {

        Class temp = HomeScreenFragment.class;
        Bundle bundle = new Bundle();
        bundle.putInt("search", 0);
        try {
            currentFragment = (Fragment) temp.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        currentFragment.setArguments(bundle);
        currentTitle = "Home";

        changeFragment(currentFragment, currentTitle);
    }

    public void changeFragment(Fragment currentFragment, String currentTitle) {

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, currentFragment)
                .addToBackStack(null)
                .commit();
        setTitle(currentTitle);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }
}
