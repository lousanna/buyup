package com.example.lousanna.buyup;


import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class ItemFragment extends Fragment {

    private View rootView;
    protected Spinner categoriesSpinner;

    public FirebaseDatabase database;
    public DatabaseReference myRef;
    public FirebaseUser user;

    public String key = "";

    public TextView title;
    public TextView price;
    public TextView condition;
    public TextView seller;
    public TextView contact;
    public TextView date;
    public ImageView img;

    public StorageReference mStorageRef;
    public StorageReference itemRef;

    public Context con;

    public ItemFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        database = FirebaseDatabase.getInstance();
        myRef = database.getReference().child("Item");

        con = getActivity().getApplicationContext();
        user = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseStorage storage = FirebaseStorage.getInstance();
        // Create a storage reference from our app
        mStorageRef = storage.getReference();

        if (getArguments().containsKey("key"))
            key = getArguments().getString("key");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_view_item, container, false);

        title = (TextView) rootView.findViewById(R.id.item_text);
        contact = (TextView) rootView.findViewById(R.id.contact_text);
        seller = (TextView) rootView.findViewById(R.id.seller_text);
        condition = (TextView) rootView.findViewById(R.id.condition_text);
        price = (TextView) rootView.findViewById(R.id.price_text);
        img = (ImageView) rootView.findViewById(R.id.imageView6);
        date = (TextView)rootView.findViewById(R.id.posted_date_text);

        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.

                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Item value = ds.getValue(Item.class);

                    if (key.equals(value.key)) {
                        if (value.date != null) {

                            DateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
                            Date curr = new Date();
                            try {
                                curr = format.parse(value.date);
                            } catch (ParseException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                            Date today = new Date();
                            long diff =  today.getTime() - curr.getTime();
                            int years = (int) ((diff / (1000 * 60 * 60 * 24))/365);
                            int days = (int) (diff / (1000 * 60 * 60 * 24));
                            int hours = (int) (diff / (1000 * 60 * 60));
                            int minutes = (int) (diff / (1000 * 60));
                            int seconds = (int) (diff / (1000));

                            System.out.println(today.toString() + "\n" + years + " " + days + " " + hours + " " + minutes + " " + seconds);
                            if (seconds == 1) {
                                date.setText("posted " + seconds + " second ago");
                            } else if (minutes < 1) {
                                date.setText("posted " + seconds + " seconds ago");
                            } else if (minutes == 1) {
                                date.setText("posted " + minutes + " minute ago");
                            } else if (hours < 1) {
                                date.setText("posted " + minutes + " minutes ago");
                            } else if (hours == 1) {
                                date.setText("posted " + hours + " hour ago");
                            } else if (days < 1) {
                                date.setText("posted " + hours + " hours ago");
                            } else if (days == 1) {
                                date.setText("posted " + days + " day ago");
                            } else if (years < 1) {
                                date.setText("posted " + days + " days ago");
                            } else if (years == 1) {
                                date.setText("posted " + years + " year ago");
                            } else {
                                date.setText("posted " + years + " years ago");
                            }

                        }
                        title.setText(value.title);

                        String c = "<i>Contact: </i>" + value.uid;
                        String s = "<i>Seller: </i>" + value.uname;
                        String p = "<i>Price: </i>$" + value.price;
                        String co = "Condition: " + value.condition;

                        contact.setText(Html.fromHtml(c));
                        seller.setText(Html.fromHtml(s));
                        price.setText(Html.fromHtml(p));
                        condition.setText(Html.fromHtml(co));

                        itemRef = mStorageRef.child(value.key + "/image.jpg");
                            // Load the image using Glide
                            Glide.with(con)
                                    .using(new FirebaseImageLoader())
                                    .load(itemRef)
                                    .asBitmap()
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .skipMemoryCache(true)
                                    .into(img);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                System.out.println("Failed to read value.");
            }
        });

        return rootView;
    }

/*
        <ImageView
            android:id="@+id/imageView8"
            android:layout_width="20dp"
            android:layout_height="20dp"
            android:layout_above="@+id/item_text"
            android:layout_centerHorizontal="true"
            app:srcCompat="@drawable/white_circle" />

        <ImageView
            android:id="@+id/imageView9"
            android:layout_width="20dp"
            android:layout_height="20dp"
            android:layout_above="@+id/item_text"
            android:layout_marginEnd="36dp"
            android:layout_marginRight="36dp"
            android:layout_toLeftOf="@+id/imageView8"
            android:layout_toStartOf="@+id/imageView8"
            app:srcCompat="@drawable/green_circle" />

        <ImageView
            android:id="@+id/imageView10"
            android:layout_width="20dp"
            android:layout_height="20dp"
            android:layout_alignBottom="@+id/imageView8"
            android:layout_marginLeft="38dp"
            android:layout_marginStart="38dp"
            android:layout_toEndOf="@+id/imageView8"
            android:layout_toRightOf="@+id/imageView8"
            app:srcCompat="@drawable/white_circle" />
 */

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }


}
