package com.example.lousanna.buyup;


import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.auth.UserProfileChangeRequest;


public class SettingsFragment extends Fragment {

    private View view;
    private FirebaseAuth mAuth;
    private FirebaseUser user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_settings, container, false);
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        String profileEmail = "";
        String profileName = "";
        if (user != null) {
            UserInfo profile = user.getProviderData().get(0);
                profileEmail = profile.getEmail();
                profileName = profile.getDisplayName();
    }

        final EditText email = (EditText)  view.findViewById(R.id.email);
        email.setText(profileEmail);
        final EditText name = (EditText)  view.findViewById(R.id.name);
        name.setText(profileName);

        Button save = (Button) view.findViewById(R.id.saveChanges);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (name.getText().toString().equals("") || email.getText().toString().equals("")) {
                    Toast.makeText(getActivity().getApplicationContext(), "Please enter a valid name/email.", Toast.LENGTH_LONG).show();
                } else {
                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                    UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                            .setDisplayName(name.getText().toString())
                            .build();

                    user.updateProfile(profileUpdates)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        //maybe print something?
                                    }
                                }
                            });

                    user.updateEmail(email.getText().toString())
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Toast.makeText(getActivity().getApplicationContext(), "Changes Saved.", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                }

            }

        });

        Button pass = (Button) view.findViewById(R.id.changePassword);
        pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                String profileEmail = "";
                if (user != null) {
                    UserInfo profile = user.getProviderData().get(0);
                    profileEmail = profile.getEmail();
                }
                EditText oldPass = (EditText)  view.findViewById(R.id.oldPass);
                final EditText newPass = (EditText)  view.findViewById(R.id.newPass);
                EditText confirmNewPass = (EditText)  view.findViewById(R.id.confirmNewPass);
                if (!confirmNewPass.getText().toString().equals(newPass.getText().toString())) {
                    Toast.makeText(getActivity().getApplicationContext(), "New Passwords Don't Match.", Toast.LENGTH_LONG).show();
                    System.out.println("pass 1" + newPass.getText().toString());
                    System.out.println("pass 2" + confirmNewPass.getText().toString());
                } else if (confirmNewPass.getText().toString().equals("") || oldPass.getText().toString().equals("")) {
                        Toast.makeText(getActivity().getApplicationContext(), "Please enter a valid password.", Toast.LENGTH_LONG).show();
                } else {
                    AuthCredential credential = EmailAuthProvider
                            .getCredential(profileEmail, oldPass.getText().toString());

                    user.reauthenticate(credential)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        user.updatePassword(newPass.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    Toast.makeText(getActivity().getApplicationContext(), "Password Changed.", Toast.LENGTH_LONG).show();
                                                } else {
                                                    Toast.makeText(getActivity().getApplicationContext(), "Password not valid.", Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        });
                                    } else {
                                        Toast.makeText(getActivity().getApplicationContext(), "Not successful.", Toast.LENGTH_LONG).show();
                                    }
                                }

                            });
                }
            }
        });
        return view;

    }
}
