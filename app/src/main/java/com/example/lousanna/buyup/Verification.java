package com.example.lousanna.buyup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * Created by emmaadams on 5/1/17.
 */

public class Verification extends AppCompatActivity {

    private View rootView;
    private ListView logListView;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private Context context;

    public Verification() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.verification);
        mAuth = FirebaseAuth.getInstance();
        context = getApplicationContext();
     //   mAuthListener = new FirebaseAuth.AuthStateListener() {
        //    @Override
         //   public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if (user.isEmailVerified()) {
                    Intent proceed = new Intent(Verification.this, HomeScreen.class);
                    startActivity(proceed);
                } else {
                    Button resend = (Button) findViewById(R.id.resend);
                    resend.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            user.sendEmailVerification()
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                Toast.makeText(getApplicationContext(), "Sent Verification Email.", Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    });


                        }
                    });
                    Button signout = (Button) findViewById(R.id.signout);
                    signout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            FirebaseAuth.getInstance().signOut();
                            Intent proceed = new Intent(Verification.this, LogIn.class);
                            startActivity(proceed);


                        }
                    });
              //  }
           // }
        };

    }
}
