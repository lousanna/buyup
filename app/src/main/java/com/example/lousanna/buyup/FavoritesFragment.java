package com.example.lousanna.buyup;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

import static java.util.Collections.sort;


public class FavoritesFragment extends Fragment {

    private View rootView;
    protected Spinner categoriesSpinner;
    private ListView logListView;
    protected static ItemAdapter aa;
    public Long pos;

    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

    public HashMap<Long, String> map = new HashMap<Long, String>();

    public FirebaseDatabase database;
    public DatabaseReference myRef;
    public ArrayList<Item> log;

    public String category;

    public FavoritesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference().child("Item");

        //myRef = database.getReference().child("Fave").child(user.getEmail());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_favorites, container, false);


        categoriesSpinner = (Spinner) rootView.findViewById(R.id.categories);
        categoriesSpinner.setPrompt("Select a category");
        final ArrayAdapter<CharSequence> categoriesAdapter = ArrayAdapter.createFromResource(this.getActivity(), R.array.categories_array, R.layout.spinner_item);
        categoriesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        categoriesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                category = (String) parent.getItemAtPosition(position);
                logListView.setAdapter(null);
                populateData(category);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                populateData("");
            }
        });

        logListView = (ListView) rootView.findViewById(R.id.log_list_view3);

        logListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Class fclass = ItemFragment.class;

                Bundle bundle = new Bundle();

                if (map.containsKey(id)) {
                    bundle.putString("key", map.get(id));
                }
                Fragment fragment = null;
                try {
                    fragment = (Fragment) fclass.newInstance();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                fragment.setArguments(bundle);

                getActivity().getFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, fragment)
                        .addToBackStack(null)
                        .commit();
                getActivity().setTitle("Item");
            }
        });



        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();

        logListView.setAdapter(null);
        populateData("");

        getActivity().setTitle("Favorites");
    }


    public void populateData(String cat) {
        log = new ArrayList<Item>();
        category = cat;
        logListView.setAdapter(null);

        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.

                log = new ArrayList<Item>();

                for(DataSnapshot ds: dataSnapshot.getChildren()) {
                    Item value = ds.getValue(Item.class);

                    if (value.getUserFave(user.getUid()) == 1 && (category.equals(value.category) || category.equals("") || category.equals("All items"))) {
                        log.add(value);

                    }
                }

                sort(log, new Comparator<Item>() {
                    @Override
                    public int compare(Item o1, Item o2) {
                        return o2.milli.compareTo(o1.milli);
                    }
                });

                for (int i = 0; i < log.size(); i++) {
                    map.put((long) i, log.get(i).key);
                }

                if (logListView.getAdapter() == null) {
                    aa = new ItemAdapter(getActivity(), R.layout.my_item_list, log);
                    logListView.setAdapter(aa);
                } else {
                    ((ItemAdapter)logListView.getAdapter()).refill(log);
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                System.out.println("Failed to read value.");
            }
        });
    }
}
