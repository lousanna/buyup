package com.example.lousanna.buyup;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.auth.UserProfileChangeRequest;

/**
 * Created by emmaadams on 4/16/17.
 */

public class EnterInformation extends AppCompatActivity {

    private View rootView;
    private ListView logListView;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    public EnterInformation() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_information);
        mAuth = FirebaseAuth.getInstance();
        Button next = (Button) findViewById(R.id.done);


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText name = (EditText) findViewById(R.id.name);
                if (name.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Please enter valid name.", Toast.LENGTH_LONG).show();
                } else {

                    final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                    UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                            .setDisplayName(name.getText().toString())
                            .build();

                    user.updateProfile(profileUpdates)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        //maybe print something?
                                    }
                                }
                            });
                    String profileEmail = "";
                    if (user != null) {
                        UserInfo profile = user.getProviderData().get(0);
                        profileEmail = profile.getEmail();
                    }
                    final EditText newPass = (EditText) findViewById(R.id.password);
                    EditText confirmNewPass = (EditText) findViewById(R.id.confirmPassword);
                    if (!confirmNewPass.getText().toString().equals(newPass.getText().toString())) {
                        Toast.makeText(getApplicationContext(), "New Passwords Don't Match.", Toast.LENGTH_LONG).show();
                    } else if (confirmNewPass.getText().toString().equals("")) {
                        Toast.makeText(getApplicationContext(), "Please enter valid password.", Toast.LENGTH_LONG).show();
                    } else {
                        AuthCredential credential = EmailAuthProvider
                                .getCredential(profileEmail, "uima2017");

                        user.reauthenticate(credential)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            user.updatePassword(newPass.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()) {
                                                        Toast.makeText(getApplicationContext(), "Account created.", Toast.LENGTH_LONG).show();
                                                        Intent proceed = new Intent(EnterInformation.this, Verification.class);
                                                        startActivity(proceed);
                                                    } else {
                                                        Toast.makeText(getApplicationContext(), "Password must be at least 6 characters.", Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            });
                                        } else {
                                            Toast.makeText(getApplicationContext(), "Not successful.", Toast.LENGTH_LONG).show();
                                        }
                                    }

                                });
                    }
                }
            }
        });

            }
        }
