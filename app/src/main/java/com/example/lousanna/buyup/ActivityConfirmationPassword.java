package com.example.lousanna.buyup;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import static com.example.lousanna.buyup.R.id.resend;

/**
 * Created by emmaadams on 4/16/17.
 */

public class ActivityConfirmationPassword extends AppCompatActivity {

    private View rootView;
    private ListView logListView;


    public ActivityConfirmationPassword() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation_code);
        Button send = (Button) findViewById(resend);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth auth = FirebaseAuth.getInstance();
                EditText email = (EditText) findViewById(R.id.email);
                String emailAddress = email.getText().toString();
                auth.sendPasswordResetEmail(emailAddress)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(getApplicationContext(), "Check email for instructions.", Toast.LENGTH_LONG).show();
                                    Intent proceed = new Intent(ActivityConfirmationPassword.this, LogIn.class);
                                    startActivity(proceed);
                                } else {
                                    Toast.makeText(getApplicationContext(), "Incorrect email.", Toast.LENGTH_LONG).show();

                                }
                            }
                        });

            }
        });

    }
}
