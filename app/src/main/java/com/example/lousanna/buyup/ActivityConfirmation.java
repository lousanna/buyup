package com.example.lousanna.buyup;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * Created by emmaadams on 4/16/17.
 */

public class ActivityConfirmation extends AppCompatActivity {

    private View rootView;
    private ListView logListView;
    private FirebaseAuth mAuth;


    public ActivityConfirmation() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation_code);
        Button resend = (Button) findViewById(R.id.resend);
        mAuth = FirebaseAuth.getInstance();
        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText email = (EditText) findViewById(R.id.email);

                if (email.getText().toString().toLowerCase().indexOf("@jhu.edu") >= 0) {

                    mAuth.createUserWithEmailAndPassword(email.getText().toString(), "uima2017")
                            .addOnCompleteListener(ActivityConfirmation.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    // If sign in fails, display a message to the user. If sign in succeeds
                                    // the auth state listener will be notified and logic to handle the
                                    // signed in user can be handled in the listener.
                                    if (!task.isSuccessful()) {
                                        Toast.makeText(getApplicationContext(), "Account already created with this email.", Toast.LENGTH_LONG).show();

                                    } else {
                                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                                        user.sendEmailVerification()
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (task.isSuccessful()) {
                                                            Toast.makeText(getApplicationContext(), "Sent Verification Email.", Toast.LENGTH_LONG).show();
                                                            Intent proceed = new Intent(ActivityConfirmation.this, EnterInformation.class);
                                                            startActivity(proceed);
                                                        }
                                                    }
                                                });

                                    }

                                    // ...
                                }
                            });


                } else {
                    Toast.makeText(getApplicationContext(), "Please enter a jhu.edu email", Toast.LENGTH_LONG).show();
                }

            }
        });

    }

}
