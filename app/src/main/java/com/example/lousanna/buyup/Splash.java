package com.example.lousanna.buyup;

/**
 * Created by lousanna on 4/16/17.
 */

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.*;
import android.view.*;
import android.widget.*;
/**
 * Created by lousanna on 4/8/17.
 */

public class Splash extends AppCompatActivity{
    
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        LinearLayout r = (LinearLayout) findViewById(R.id.splash);

        r.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent proceed = new Intent(Splash.this, LogIn.class);
                startActivity(proceed);
            }
        });

    }
}