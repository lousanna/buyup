package com.example.lousanna.buyup;

/**
 * Created by lousanna on 4/16/17.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static com.example.lousanna.buyup.R.id.photo;

public class AddItemFragment extends Fragment {

    private View view;
    private Context con;
    private DatabaseReference mDatabase;
    private FirebaseUser user;


    private ImageView img;
    private EditText title;
    private EditText price;
    private Spinner size;
    private Spinner color;
    private Spinner category;
    private Spinner condition;
    private int favorite = 0;
    private StorageReference itemRef;
    private StorageReference mStorageRef;
    private String itemKey;

    private Fragment fg;
    private Item finalvalue;

    public Long milli;

    private boolean upload = false;

    private Button list;
    private Button remove;

    private String position;

    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    private static final int SELECT_IMAGE = 1234;

    public AddItemFragment (){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        con = getActivity().getApplicationContext();

        user = FirebaseAuth.getInstance().getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        FirebaseStorage storage = FirebaseStorage.getInstance();

        // Create a storage reference from our app
        mStorageRef = storage.getReference();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        // super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
                if (resultCode == Activity.RESULT_OK && data != null) {

                    Bitmap bmp = (Bitmap) data.getExtras().get("data");

                    upload = true;
                    System.out.println("passed");
                    img.setImageBitmap(bmp);
                }
            } else if (requestCode == SELECT_IMAGE) {
                if (resultCode == Activity.RESULT_OK && data != null) {

                    upload = true;
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = con.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String filePath = cursor.getString(columnIndex);
                    cursor.close();


                    Bitmap bmp = BitmapFactory.decodeFile(filePath);
                    img.setImageBitmap(bmp);
                }
            }
        }catch(Exception e){
            Toast.makeText(this.getActivity(), e+"Error", Toast.LENGTH_LONG).show();

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_add_item, container, false);

        img = (ImageView) view.findViewById(photo);
        title = (EditText) view.findViewById(R.id.titleAdd);
        price = (EditText) view.findViewById(R.id.priceAdd);
        size = (Spinner) view.findViewById(R.id.sizeSpin);
        color = (Spinner) view.findViewById(R.id.colorSpin);
        category = (Spinner) view.findViewById(R.id.categorySpin);
        condition = (Spinner) view.findViewById(R.id.conditionSpin);
        list = (Button) view.findViewById(R.id.listAdd);
        remove = (Button) view.findViewById(R.id.listRemove);

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder getImageFrom = new AlertDialog.Builder(getActivity());
                getImageFrom.setTitle("Select:");
                final CharSequence[] opsChars = {getResources().getString(R.string.takepic), getResources().getString(R.string.opengallery)};
                getImageFrom.setItems(opsChars, new android.content.DialogInterface.OnClickListener(){


                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == 0){
                            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            getActivity().startActivityFromFragment(AddItemFragment.this, cameraIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                        } else
                        if(which == 1){
                            Intent intent = new Intent(Intent.ACTION_PICK,
                                    android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);

                            if (ActivityCompat.checkSelfPermission(con,
                                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                                    ActivityCompat.checkSelfPermission(con,
                                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions(getActivity(),
                                        new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        786);
                            } else {
                                getActivity().startActivityFromFragment(AddItemFragment.this, Intent.createChooser(intent,
                                        "Choose picture"), SELECT_IMAGE);
                            }

                        }
                        dialog.dismiss();
                    }
                });
                getImageFrom.show();
            }
        });

        list.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                final String uid = user.getEmail();
                final String uname = user.getDisplayName();
                final String titleText = title.getText().toString();
                final String priceText = price.getText().toString();
                final String sizeChoice = size.getSelectedItem().toString();
                final String colorChoice = color.getSelectedItem().toString();
                final String categoryChoice = category.getSelectedItem().toString();
                final String conditionChoice = condition.getSelectedItem().toString();

                if(titleText.equals("") || priceText.equals("") || sizeChoice.equals("") || sizeChoice.equals("Size") || colorChoice.equals("Color") || colorChoice.equals("") || categoryChoice.equals("Category") || categoryChoice.equals("") || conditionChoice.equals("") || conditionChoice.equals("Condition"))
                    Toast.makeText(getActivity(), "Please enter all fields.", Toast.LENGTH_LONG).show();
                else {
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Confirmation")
                            .setMessage("This listing will be added.")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    int fave = 0;

                                    writeNewItem(titleText, priceText, sizeChoice, colorChoice, categoryChoice, conditionChoice, fave, uid, uname);

                                    Toast.makeText(getActivity(), "Added Listing. Redirecting..", Toast.LENGTH_SHORT).show();

                                    Class fclass = MyItemsFragment.class;

                                    Bundle bundle = new Bundle();
                                    bundle.putInt("from", 1);
                                    fg = null;
                                    try {
                                        fg = (Fragment) fclass.newInstance();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    fg.setArguments(bundle);
                                    Handler handler = new Handler();
                                    Runnable runnable = new Runnable() {
                                        @Override
                                        public void run() {
                                            //Second fragment after 5 seconds appears
                                            getActivity().getFragmentManager().beginTransaction()
                                                    .replace(R.id.content_frame, fg)
                                                    .addToBackStack(null)
                                                    .commit();
                                            getActivity().setTitle("My Items");
                                        }
                                    };

                                    handler.postDelayed(runnable, 1000);


                                }

                            })
                            .setNegativeButton("No", null)
                            .show();
                }
            }
        });
        List<Object> obj1 = new ArrayList<Object>(Arrays.asList(getResources().getStringArray(R.array.size)));
        HintAdapter adapter1 = new HintAdapter(con, R.layout.spinner_item, obj1);
        adapter1.setDropDownViewResource(R.layout.spinner_dropdown_item);
        size.setAdapter(adapter1);
        size.setSelection(6);

        List<Object> obj2 = new ArrayList<Object>(Arrays.asList(getResources().getStringArray(R.array.color)));
        HintAdapter adapter2 = new HintAdapter(con, R.layout.spinner_item, obj2);
        adapter2.setDropDownViewResource(R.layout.spinner_dropdown_item);
        color.setAdapter(adapter2);
        color.setSelection(13);

        List<Object> obj3 = new ArrayList<Object>(Arrays.asList(getResources().getStringArray(R.array.category)));
        HintAdapter adapter3 = new HintAdapter(con, R.layout.spinner_item, obj3);
        adapter3.setDropDownViewResource(R.layout.spinner_dropdown_item);
        category.setAdapter(adapter3);
        category.setSelection(6);

        List<Object> obj4 = new ArrayList<Object>(Arrays.asList(getResources().getStringArray(R.array.condition)));
        HintAdapter adapter4 = new HintAdapter(con, R.layout.spinner_item, obj4);
        adapter4.setDropDownViewResource(R.layout.spinner_dropdown_item);
        condition.setAdapter(adapter4);
        condition.setSelection(8);



        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        upload = false;
    }

    @Override
    public void onResume() {
        super.onResume();

        Integer editable = getArguments().getInt("edit");

        if (editable == 1) {
            position = getArguments().getString("position");
            list.setText("Update Listing");

            getItem();

            list.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    final String uid = user.getEmail();
                    final String uname = user.getDisplayName();
                    final String titleText = title.getText().toString();
                    final String priceText = price.getText().toString();
                    final String sizeChoice = size.getSelectedItem().toString();
                    final String colorChoice = color.getSelectedItem().toString();
                    final String categoryChoice = category.getSelectedItem().toString();
                    final String conditionChoice = condition.getSelectedItem().toString();
                    final int fave = favorite;

                    if(titleText.equals("") || priceText.equals("")) {
                        Toast.makeText(getActivity(), "Please enter all fields.", Toast.LENGTH_LONG).show();
                    } else{
                        new AlertDialog.Builder(getActivity())
                                .setTitle("Confirmation")
                                .setMessage("This listing will be edited.")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        updateItem(titleText, priceText, sizeChoice, colorChoice, categoryChoice, conditionChoice, fave, position, uid, uname);

                                        Toast.makeText(getActivity(), "Edited Listing. Redirecting..", Toast.LENGTH_SHORT).show();

                                        Class fclass = MyItemsFragment.class;

                                        Bundle bundle = new Bundle();
                                        bundle.putInt("from", 1);
                                        fg = null;
                                        try {
                                            fg = (Fragment) fclass.newInstance();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        fg.setArguments(bundle);
                                        Handler handler = new Handler();
                                        Runnable runnable = new Runnable() {
                                            @Override
                                            public void run() {
                                                //Second fragment after 5 seconds appears
                                                getActivity().getFragmentManager().beginTransaction()
                                                        .replace(R.id.content_frame, fg)
                                                        .addToBackStack(null)
                                                        .commit();
                                                getActivity().setTitle("My Items");
                                            }
                                        };

                                        handler.postDelayed(runnable, 1000);

                                    }

                                })
                                .setNegativeButton("No", null)
                                .show();
                    }
                }
            });
            remove.setVisibility(View.VISIBLE);
            remove.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    new AlertDialog.Builder(getActivity())
                            .setTitle("Confirmation")
                            .setMessage("This listing will be removed.")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
                                    Query query = ref.child("Item").child(position);

                                    query.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            for (DataSnapshot s: dataSnapshot.getChildren()) {
                                                s.getRef().removeValue();
                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                            System.out.println("onCancelled");
                                        }
                                    });

                                    query.getRef().removeValue();

                                    StorageReference sRef = mStorageRef.child(position + "/image.jpg");

                                    sRef.delete();

                                    Class fclass = MyItemsFragment.class;

                                    Bundle bundle = new Bundle();
                                    bundle.putInt("from", 0);
                                    Fragment fragment = null;
                                    try {
                                        fragment = (Fragment) fclass.newInstance();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    fragment.setArguments(bundle);

                                    getActivity().getFragmentManager().beginTransaction()
                                            .replace(R.id.content_frame, fragment)
                                            .addToBackStack(null)
                                            .commit();
                                    getActivity().setTitle("My Items");
                                }

                            })
                            .setNegativeButton("No", null)
                            .show();
                }
            });
        } else if (editable == 2) {
            list.setText("Search");
            img.setVisibility(View.GONE);
            list.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    Class fclass = HomeScreenFragment.class;

                    Bundle bundle = new Bundle();

                    String titleText = title.getText().toString();
                    String priceText = price.getText().toString();
                    String sizeChoice = size.getSelectedItem().toString();
                    String colorChoice = color.getSelectedItem().toString();
                    String categoryChoice = category.getSelectedItem().toString();
                    String conditionChoice = condition.getSelectedItem().toString();

                    bundle.putString("title", titleText);
                    bundle.putString("price", priceText);
                    bundle.putString("size", sizeChoice);
                    bundle.putString("color", colorChoice);
                    bundle.putString("category", categoryChoice);
                    bundle.putString("condition", conditionChoice);
                    bundle.putInt("search", 1);

                    Fragment fragment = null;
                    try {
                        fragment = (Fragment) fclass.newInstance();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    fragment.setArguments(bundle);

                    getActivity().getFragmentManager().beginTransaction()
                            .replace(R.id.content_frame, fragment)
                            .addToBackStack(null)
                            .commit();
                    getActivity().setTitle("Search Results");
                }
            });
        }
    }

    public void getItem() {
        // Read from the database

        DatabaseReference myRef = mDatabase.child("Item");
        myRef.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.

                for(DataSnapshot ds: dataSnapshot.getChildren()) {


                    Item value = ds.getValue(Item.class);

                    if (value.key != null && value.key.equals(position)) {
                        title.setText(value.title);
                        price.setText(value.price);

                        category.setSelection(((ArrayAdapter<String>) category.getAdapter()).getPosition(value.category));
                        condition.setSelection(((ArrayAdapter<String>) condition.getAdapter()).getPosition(value.condition));

                        size.setSelection(((ArrayAdapter<String>) size.getAdapter()).getPosition(value.size));
                        color.setSelection(((ArrayAdapter<String>) color.getAdapter()).getPosition(value.color));

                        itemRef = mStorageRef.child(value.key + "/image.jpg");

                        //favorite = value.fave;
                        favorite = value.getUserFave(user.getUid());

                        milli = value.milli;

                        finalvalue = value;


                        if(!upload) {
                            // Load the old image using Glide
                            Glide.with(con)
                                    .using(new FirebaseImageLoader())
                                    .load(itemRef)
                                    .asBitmap()
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .skipMemoryCache(true)
                                    .into(img);
                            System.out.println("glide");
                        }

                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                System.out.println("Failed to read value.");
            }
        });
    }

    private String updateItem(String titleText, String priceText, String size, String color, String categoryChoice,
                            String conditionChoice, int fave, String key, String uid, String uname) {

        Date d = new Date();
        DateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
        String date = format.format(d);

        System.out.println("Time: " + milli + " " + d.getTime());
        Item item = new Item(titleText, priceText, size, color, categoryChoice, conditionChoice, fave, 0, uid, uname, date, d.getTime());

        item.deepcopyMap(finalvalue);
        item.key = new String(finalvalue.key);
        itemKey = new String(item.key);

        DatabaseReference itemRef = mDatabase.child("Item").child(key);
        itemRef.setValue(item);

        HashMap<String, Object> itemUpdates = new HashMap<>();
        itemUpdates.put("/Item/" + key, item);
        mDatabase.updateChildren(itemUpdates);

        getPhoto(item.key, item);

        return itemKey;
    }

    public void changeFragment(Class c, String t) {

        Fragment fragment = null;
        try {
            fragment = (Fragment) c.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        getActivity().getFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).addToBackStack(null).commit();

        getActivity().setTitle(t);
    }


    private String writeNewItem(String titleText, String priceText, String size, String color, String categoryChoice,
                              String conditionChoice, int fave, String uid, String uname) {

        Date d = new Date();
        DateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
        String date = format.format(d);

        Item item = new Item(titleText, priceText, size, color, categoryChoice, conditionChoice, fave, 0, uid, uname, date, d.getTime());

        DatabaseReference usersRef = mDatabase.child("Item").push();
        itemKey = usersRef.getKey();
        item.key = itemKey;

        usersRef.setValue(item);
        getPhoto(item.key, item);
        return itemKey;
    }

    private UploadTask getPhoto(String itemKey, Item value) {
        itemRef = mStorageRef.child(itemKey + "/image.jpg");

        img.setDrawingCacheEnabled(true);
        img.buildDrawingCache();
        Bitmap bitmap = img.getDrawingCache();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = itemRef.putBytes(data);

        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size,
                // content-type, and download URL.
                @SuppressWarnings("VisibleForTests") Uri downloadUrl = taskSnapshot.getDownloadUrl();

            }
        });

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                Glide.get(con).clearDiskCache();
            }
        });

        return uploadTask;
    }
}

class HintAdapter extends ArrayAdapter<Object> {

    public HintAdapter(Context con,int layout, List<Object> objects) {
        super(con, layout, objects);
    }

    @Override
    public int getCount() {
        // don't display last item. It is used as hint.
        int count = super.getCount();
        return count > 0 ? count - 1 : count;
    }
}
