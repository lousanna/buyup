package com.example.lousanna.buyup;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.StringSignature;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.HashMap;
import java.util.List;

/**
 * Created by emmaadams on 4/16/17.
 */

public class ItemAdapter extends ArrayAdapter<Item> {

    int res;

    private Context con;

    ViewHolder holder;

    public DatabaseReference mDatabase;
    public StorageReference mStorageRef;

    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();


    public ItemAdapter(Context ctx, int res, List<Item> items)  {
        super(ctx, res, items);
        this.con = ctx;
        this.res = res;
    }

    class ViewHolder {
        TextView titleView;
        TextView conditionView;
        TextView categoryView;
        TextView priceView;
        ImageView fave;
        ImageView img;

        Item iLog;
    }

    public void refill(List<Item> events) {

        this.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        holder = new ViewHolder();


        FirebaseStorage storage = FirebaseStorage.getInstance();
        // Create a storage reference from our app
        mStorageRef = storage.getReference();

        if (v == null) {
            LayoutInflater vi = (LayoutInflater)con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(res, null);

            holder = new ViewHolder();
            holder.titleView = (TextView) v.findViewById(R.id.title);
            holder.conditionView = (TextView) v.findViewById(R.id.condition);
            holder.categoryView = (TextView) v.findViewById(R.id.category);
            holder.priceView = (TextView) v.findViewById(R.id.price);
            holder.fave = (ImageView) v.findViewById(R.id.fave);
            holder.img = (ImageView) v.findViewById(R.id.imageSnap);

            holder.iLog = getItem(position);
            v.setTag(holder);
        }
        else {
            holder = (ViewHolder)v.getTag();
        }

        final ImageView fave = (ImageView) v.findViewById(R.id.fave);
        final Item iLog = getItem(position);
        final Long mill = getItem(position).milli;

        String title = iLog.title;
        String category = iLog.category;
        String price = iLog.price;
        String condition = iLog.condition;

        if (iLog.getUserFave(user.getUid()) == 0) {
            fave.setActivated(false);
        } else {
            fave.setActivated(true);
        }

        fave.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                //int curr = iLog.fave;
                if (!fave.isActivated()) {

                    fave.setActivated(true);
                    updateFave(iLog, 1, iLog.key);
                } else {
                    //fave.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                    fave.setActivated(false);
                    updateFave(iLog, 0, iLog.key);
                }
                //iLog.fave = Math.abs(curr - 1);
            }
        });

        holder.titleView.setText(title);
        holder.categoryView.setText(category);
        holder.conditionView.setText(condition);
        holder.priceView.setText("$" + price);

        StorageReference itemRef = mStorageRef.child(iLog.key + "/image.jpg");

            // Load the image using Glide
            Glide.with(con)
                    .using(new FirebaseImageLoader())
                    .load(itemRef)
                    .signature(new StringSignature(iLog.date + iLog.milli))
                    .error(R.drawable.camera)
                    .into(holder.img);

        System.out.println(mill);

        return v;
    }

    private void updateFave(Item iLog, int fave, String key) {

        iLog.toggleUserFave(user.getUid());

        mDatabase = FirebaseDatabase.getInstance().getReference();

        DatabaseReference itemRef = mDatabase.child("Item").child(key);
        itemRef.setValue(iLog);
        HashMap<String, Object> itemUpdates = new HashMap<>();
        itemUpdates.put("/Item/" + key, iLog);
        mDatabase.updateChildren(itemUpdates);
    }

}