package com.example.lousanna.buyup;


import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Comparator;
import static java.util.Collections.sort;



public class HomeScreenFragment extends Fragment {

    private View rootView;
    protected Spinner categoriesSpinner;
    private ListView logListView;
    public Long pos;
    public HashMap<Long, String> map = new HashMap<Long, String>();
    protected static ItemAdapter aa;

    public FirebaseDatabase database;
    public DatabaseReference myRef;
    public FirebaseUser user;

    public String category;

    public ArrayList<Item> log;

    private String t;
    private String p;
    private String s;
    private String c;
    private String cat;
    private String cond;


    public HomeScreenFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference().child("Item");


        user = FirebaseAuth.getInstance().getCurrentUser();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_home_screen, container, false);


        categoriesSpinner = (Spinner) rootView.findViewById(R.id.categories);
        categoriesSpinner.setPrompt("Select a category");
        category = "";

        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Class fclass = AddItemFragment.class;

                Bundle bundle = new Bundle();
                bundle.putInt("edit", 0);
                Fragment fragment = null;
                try {
                    fragment = (Fragment) fclass.newInstance();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                fragment.setArguments(bundle);

                getActivity().getFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, fragment)
                        .addToBackStack(null)
                        .commit();
                getActivity().setTitle("Add a Listing");
            }
        });

        final ArrayAdapter<CharSequence> categoriesAdapter = ArrayAdapter.createFromResource(this.getActivity(), R.array.categories_array, R.layout.spinner_item);
        categoriesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        categoriesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                category = (String) parent.getItemAtPosition(position);
                if (! (getArguments().containsKey("search") && getArguments().getInt("search") == 1)) {
                    logListView.setAdapter(null);
                    populateData(category);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                populateData("");
            }
        });

        logListView = (ListView) rootView.findViewById(R.id.log_list_view2);

        logListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Class fclass = ItemFragment.class;

                Bundle bundle = new Bundle();

                if (map.containsKey(id)) {
                    bundle.putString("key", map.get(id));
                }
                Fragment fragment = null;
                try {
                    fragment = (Fragment) fclass.newInstance();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                fragment.setArguments(bundle);

                getActivity().getFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, fragment)
                        .addToBackStack(null)
                        .commit();
                getActivity().setTitle("Item");
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        // Populating fake data - will need to replace with getting actual data from database
        //populateData("");

        Bundle bundle = getArguments();
        if(bundle.containsKey("search") && bundle.getInt("search") == 1) {

            categoriesSpinner.setVisibility(View.GONE);

            t = bundle.getString("title");
            p = bundle.getString("price");
            s = bundle.getString("size");
            c = bundle.getString("color");
            cat = bundle.getString("category");
            cond = bundle.getString("condition");

            searchData();

        } else {
            populateData("");
            getActivity().setTitle("Home");
        }
    }

    public void searchData() {

        logListView.setAdapter(null);
        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.

                log = new ArrayList<Item>();
                pos = new Long(0);

                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Item value = ds.getValue(Item.class);

                    t = t.toLowerCase();
                    if(!t.equals("") && !value.title.toLowerCase().contains(t))
                        continue;
                    if (p != null && !p.equals("") && isNumeric(p) && value.price != null && !value.price.equals("") && isNumeric(value.price) && Math.abs(Double.parseDouble(value.price) - Double.parseDouble(p)) > 10 )
                        continue;
                    if(!value.size.equals(s) && !s.equals("Size"))
                        continue;
                    if(!value.color.equals(c) && !c.equals("Color"))
                        continue;
                    if(!value.category.equals(cat) && !cat.equals("Category"))
                        continue;
                    if(!value.condition.equals(cond) && !cond.equals("Condition"))
                        continue;

                    log.add(value);

                    map.put(pos, value.key);
                    pos++;
                }

                TextView showEmpty = (TextView) rootView.findViewById(R.id.empty);
                if (log.isEmpty()) {
                    showEmpty.setVisibility(View.VISIBLE);
                } else {
                    showEmpty.setVisibility(View.GONE);
                }

                sort(log, new Comparator<Item>() {
                    @Override
                    public int compare(Item o1, Item o2) {
                        return o2.milli.compareTo(o1.milli);
                    }
                });

                for (int i = 0; i < log.size(); i++) {
                    map.put((long) i, log.get(i).key);
                }

                if (logListView.getAdapter() == null) {
                    aa = new ItemAdapter(getActivity(), R.layout.my_item_list, log);
                    logListView.setAdapter(aa);
                } else {
                    ((ItemAdapter)logListView.getAdapter()).refill(log);
                }

            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                System.out.println("Failed to read value.");
            }
        });
    }

    public static boolean isNumeric(String str)
    {
        try
        {
            double d = Double.parseDouble(str);
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }


    public void populateData(String cat) {

        category = new String(cat);
        log = new ArrayList<Item>();
        //logListView.setAdapter(null);

        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.

                //logListView.setAdapter(null);
                log = new ArrayList<Item>();
                pos = new Long(0);

                for(DataSnapshot ds: dataSnapshot.getChildren()) {
                    Item value = ds.getValue(Item.class);
                    if (category.equals(value.category) || category.equals("") || category.equals("All items")) {

                        log.add(value);

                        map.put(pos, value.key);
                        pos++;

                    }

                }

                sort(log, new Comparator<Item>() {
                    @Override
                    public int compare(Item o1, Item o2) {
                        return o2.milli.compareTo(o1.milli);
                    }
                });

                for (int i = 0; i < log.size(); i++) {
                    map.put((long) i, log.get(i).key);
                }



                if (logListView.getAdapter() == null) {
                    aa = new ItemAdapter(getActivity(), R.layout.my_item_list, log);
                    logListView.setAdapter(aa);
                } else {
                    ((ItemAdapter)logListView.getAdapter()).refill(log);
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                System.out.println("Failed to read value.");
            }
        });
/*
        String[] title = new String[6];
        title[0] = "Black Couch";
        title[1] = "Jeans";
        title[2] = "Math Text Book";
        title[3] = "Dress";
        title[4] = "Make-up";
        title[5] = "Bed";

        String[] price = new String[6];
        price[0] = "$100";
        price[1] = "$11.00";
        price[2] = "$12.00";
        price[3] = "$3.00";
        price[4] = "$50.00";
        price[5] = "$100.00";

        String[] category = new String[6];
        category[0] = "Furniture";
        category[1] = "Clothes";
        category[2] = "School";
        category[3] = "Clothes";
        category[4] = "Make-up";
        category[5] = "Furniture";

        String[] condition = new String[6];
        condition[0] = "Good";
        condition[1] = "Gently Used";
        condition[2] = "Used";
        condition[3] = "New";
        condition[4] = "Gently Used";
        condition[5] = "New";


        int[] favorite = {0,1,0,1,1,0};

        for(int i = 0; i < title.length; i++) {
            Item temp = new Item(title[i], price[i], category[i], condition[i], favorite[i], null, null);
            log.add(temp);
        }


*/

    }



    //@Override
    // public View onCreateView(LayoutInflater inflater, ViewGroup container,
    //Bundle savedInstanceState) {
    // Inflate the layout for this fragment
        /*rootView = inflater.inflate(R.layout.fragment_new_lesson, container, false);
        drivelog = new DriveLog();
        dateText = (TextView) rootView.findViewById(R.id.date);
        hoursText = (EditText) rootView.findViewById(R.id.hours);

        hoursText.setText("");
        dateText.setText("");

        hoursText.setEnabled(false);

        lessonSpinner = (Spinner) rootView.findViewById(R.id.lesson_spinner);
        weatherSpinner = (Spinner) rootView.findViewById(R.id.weather_spinner);

        dayButton = (RadioButton) rootView.findViewById(R.id.day_radio_btn);
        nightButton = (RadioButton) rootView.findViewById(R.id.night_radio_btn);

        datePicker = (ImageButton) rootView.findViewById(R.id.date_picker);
        datePicker.setVisibility(View.GONE);

        datePicker.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });

        startButton = (Button) rootView.findViewById(R.id.start_button);
        stopButton = (Button) rootView.findViewById(R.id.stop_button);
        saveButton = (Button) rootView.findViewById(R.id.save_button);
        cancelButton = (Button) rootView.findViewById(R.id.cancel_button);

        stopButton.setEnabled(false);
        saveButton.setEnabled(false);
        cancelButton.setEnabled(false);

        startButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                start();
            }
        });

        stopButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                stop();
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                save();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                cancel();
            }
        });

        final ArrayAdapter<CharSequence> lessonAdapter = ArrayAdapter.createFromResource(this.getActivity(),
                R.array.lesson_array, R.layout.spinner_item);
        lessonAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        final ArrayAdapter<CharSequence> weatherAdapter = ArrayAdapter.createFromResource(this.getActivity(),
                R.array.weather_array, R.layout.spinner_item);
        weatherAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        lessonSpinner.setAdapter(lessonAdapter);
        weatherSpinner.setAdapter(weatherAdapter);

        return rootView;*/
    // }





    @Override
    public void onStart() {
        super.onStart();
    }








}